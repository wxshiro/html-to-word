package com.xuwangcheng.html2word.handler;

import com.deepoove.poi.data.style.Style;
import com.xuwangcheng.html2word.HandlerParams;

/**
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2022/3/1 11:28
 */
public class ThTagHandler extends BaseHtmlTagHandler {
    @Override
    public String getMatchTagName() {
        return "th";
    }

    @Override
    public void handleHtmlElement(HandlerParams params, Style style) {
        style.setUnderLine(true);
    }
}
